# French translation for the GNOME initial setup
# Copyright (C) 2012-2022 listed translators
#
# Alexandre Franke <alexandre.franke@gmail.com>, 2012.
# Christophe Fergeau <teuf@gnome.org>, 2013.
# Baptiste Mille-Mathias <baptistem@gnome.org>, 2013.
# Alain Lojewski <allomervan@gmail.com>, 2014-2015.
# naybnet <naybnet@gmail.com>, 2014.
# Guillaume Bernard <associations@guillaume-bernard.fr>, 2014-2018.
# Claude Paroz <claude@2xlibre.net>, 2016-2022.
# Thibault Martin <mail@thibaultmart.in>, 2020.
# Charles Monzat <charles.monzat@free.fr>, 2016-2022.
# Irénée THIRION <irenee.thirion@e.email>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-initial-setup\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-initial-setup/"
"issues\n"
"POT-Creation-Date: 2023-03-03 20:19+0000\n"
"PO-Revision-Date: 2023-03-03 18:12+0100\n"
"Last-Translator: Irénée THIRION <irenee.thirion@e.email>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-DL-Team: fr\n"
"X-DL-Module: gnome-initial-setup\n"
"X-DL-Branch: master\n"
"X-DL-Domain: po\n"
"X-DL-State: Translating\n"
"X-Generator: Gtranslator 42.0\n"

#: data/gnome-initial-setup-first-login.desktop.in.in:3
#: data/gnome-initial-setup.desktop.in.in:3
msgid "Initial Setup"
msgstr "Configuration initiale"

#: gnome-initial-setup/gis-assistant.c:403
msgid "_Next"
msgstr "Suiva_nt"

#: gnome-initial-setup/gis-assistant.c:404
msgid "_Accept"
msgstr "_Accepter"

#: gnome-initial-setup/gis-assistant.c:405
msgid "_Skip"
msgstr "Pa_sser"

#: gnome-initial-setup/gis-assistant.c:406
msgid "_Previous"
msgstr "_Précédent"

#: gnome-initial-setup/gis-assistant.c:407
msgid "_Cancel"
msgstr "A_nnuler"

#: gnome-initial-setup/gnome-initial-setup.c:282
msgid "Force existing user mode"
msgstr "Forcer le mode utilisateur existant"

#: gnome-initial-setup/gnome-initial-setup.c:296
msgid "— GNOME initial setup"
msgstr "— Configuration initiale de GNOME"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:200
msgid "Failed to register account"
msgstr "Échec lors de l’enregistrement du compte"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:389
msgid "No supported way to authenticate with this domain"
msgstr "Aucune méthode prise en charge pour authentifier avec ce domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:428
msgid "Failed to join domain"
msgstr "Échec lors de la prise de contact avec le domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:495
msgid "Failed to log into domain"
msgstr "Échec lors de la connexion au domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:12
msgid "Enterprise Login"
msgstr "Connexion d’entreprise"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:13
msgid ""
"Enterprise login allows an existing centrally managed user account to be "
"used on this device. You can also use this account to access company "
"resources on the internet."
msgstr ""
"Une connexion d’entreprise autorise l’utilisation sur ce périphérique d’un "
"compte utilisateur global centralisé. Vous pouvez également utiliser ce "
"compte pour accéder à des ressources d’entreprise sur Internet."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:27
#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:153
msgid "_Domain"
msgstr "_Domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:40
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:81
msgid "_Username"
msgstr "Nom d’_utilisateur"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:53
#: gnome-initial-setup/pages/password/gis-password-page.ui:25
msgid "_Password"
msgstr "Mot de _passe"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:99
msgid "Enterprise domain or realm name"
msgstr "Domaine d’entreprise ou nom du domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:126
msgid "Domain Administrator Login"
msgstr "Connexion d’administrateur de domaine"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:141
msgid ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password "
"here, and choose a unique computer name for your computer."
msgstr ""
"Afin d’utiliser des connexions d’entreprise, cet ordinateur doit être "
"enregistré dans un domaine. Demandez à votre administrateur réseau de saisir "
"ici le mot de passe du domaine et choisissez un nom unique pour votre "
"ordinateur."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:179
msgid "_Computer"
msgstr "Or_dinateur"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:203
msgid "Administrator _Name"
msgstr "_Nom de l’administrateur"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:227
msgid "Administrator Password"
msgstr "Mot de passe de l’administrateur"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:260
msgid "C_ontinue"
msgstr "C_ontinuer"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:207
msgid "Please check the name and username. You can choose a picture too."
msgstr ""
"Merci de vérifier votre nom et votre nom de compte. Vous pouvez également "
"choisir un avatar."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:459
msgid "We need a few details to complete setup."
msgstr "Il nous faut quelques détails pour terminer la configuration."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:555
msgid "Administrator"
msgstr "Administrateur"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:561
#: gnome-initial-setup/pages/account/gis-account-page-local.c:604
#, c-format
msgid "Failed to create user '%s': "
msgstr "Impossible de créer l’utilisateur « %s » : "

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:30
msgid "Edit avatar"
msgstr "Modifier l’avatar"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:45
#: gnome-initial-setup/pages/account/gis-account-page.c:274
msgid "About You"
msgstr "À propos de vous"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:46
msgid "Please provide a name and username. You can choose a picture too."
msgstr ""
"Merci d’indiquer votre nom et votre nom de compte. Vous pouvez également "
"choisir un avatar."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:58
msgid "_Full Name"
msgstr "No_m complet"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:133
msgid "Set up _parental controls for this user"
msgstr "Configurer les contrôles _parentaux pour cet utilisateur"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:139
msgid "For use by a parent or supervisor, who must set up their own password."
msgstr ""
"À l’usage d’un parent ou d’un superviseur, qui doit configurer son propre "
"mot de passe."

#: gnome-initial-setup/pages/account/gis-account-page.ui:32
msgid "_Enterprise Login"
msgstr "Connexion d’_entreprise"

#: gnome-initial-setup/pages/account/gis-account-page.ui:41
msgid "Go online to set up Enterprise Login."
msgstr "Configurez en ligne la connexion d’entreprise."

#: gnome-initial-setup/pages/account/um-realm-manager.c:310
msgid "Cannot automatically join this type of domain"
msgstr "Impossible de rejoindre automatiquement ce type de domaine"

#: gnome-initial-setup/pages/account/um-realm-manager.c:373
#, c-format
msgid "No such domain or realm found"
msgstr "Aucun domaine correspondant trouvé"

#: gnome-initial-setup/pages/account/um-realm-manager.c:782
#: gnome-initial-setup/pages/account/um-realm-manager.c:796
#, c-format
msgid "Cannot log in as %s at the %s domain"
msgstr "Impossible de se connecter en tant que %s au domaine %s"

#: gnome-initial-setup/pages/account/um-realm-manager.c:788
msgid "Invalid password, please try again"
msgstr "Mot de passe non valide, veuillez réessayer"

#: gnome-initial-setup/pages/account/um-realm-manager.c:801
#, c-format
msgid "Couldn’t connect to the %s domain: %s"
msgstr "Impossible de se connecter au domaine %s : %s"

#: gnome-initial-setup/pages/account/um-utils.c:153
msgid "Sorry, that user name isn’t available. Please try another."
msgstr ""
"Désolé, ce nom d’utilisateur n’est pas disponible. Essayez-en un autre."

#: gnome-initial-setup/pages/account/um-utils.c:156
#, c-format
msgid "The username is too long."
msgstr "Le nom d’utilisateur est trop long."

#: gnome-initial-setup/pages/account/um-utils.c:159
msgid "The username must start with a lower case letter from a-z."
msgstr "Le nom d’utilisateur doit commencer par une lettre minuscule de a à z."

#: gnome-initial-setup/pages/account/um-utils.c:162
msgid "That username isn’t available. Please try another."
msgstr "Ce nom d’utilisateur n’est pas disponible. Essayez-en un autre."

#: gnome-initial-setup/pages/account/um-utils.c:165
msgid ""
"The username should only consist of lower case letters from a-z, digits, and "
"the following characters: - _"
msgstr ""
"Le nom d’utilisateur doit être uniquement composé de lettres minuscules de a "
"à z, de nombres et des caractères suivants : - _"

#: gnome-initial-setup/pages/account/um-utils.c:169
msgid "This will be used to name your home folder and can’t be changed."
msgstr ""
"Il sera utilisé pour nommer votre dossier personnel et ne pourra pas être "
"changé."

#: gnome-initial-setup/pages/goa/gis-goa-page.c:561
msgid "Online Accounts"
msgstr "Comptes en ligne"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:12
msgid "Connect Your Online Accounts"
msgstr "Connecter vos comptes en ligne"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:13
msgid ""
"Connect your accounts to easily access your email, online calendar, "
"contacts, documents and photos."
msgstr ""
"Connectez vos comptes pour accéder facilement à vos courriels, votre agenda "
"en ligne, vos contacts, documents et photos."

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:40
msgid ""
"Accounts can be added and removed at any time from the Settings application."
msgstr ""
"Les comptes peuvent être ajoutés et supprimés à tout moment dans les "
"Paramètres."

#. translators: This is the title of the "Show Account" dialog. The
#. * %s is the name of the provider. e.g., 'Google'.
#: gnome-initial-setup/pages/goa/gnome-initial-setup-goa-helper.c:428
#, c-format
msgid "%s Account"
msgstr "Compte %s"

#: gnome-initial-setup/pages/goa/gnome-initial-setup-goa-helper.c:431
msgid "Remove Account"
msgstr "Enlever le compte"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:236
msgid "Preview"
msgstr "Aperçu"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:303
#: gnome-initial-setup/pages/language/cc-language-chooser.c:222
msgid "More…"
msgstr "Plus…"

#. Translators: a search for input methods or keyboard layouts
#. * did not yield any results
#.
#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:325
msgid "No inputs found"
msgstr "Aucune méthode de saisie trouvée"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.c:502
#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:15
msgid "Typing"
msgstr "Saisie"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:16
msgid "Select your keyboard layout or an input method."
msgstr "Choisissez la disposition du clavier ou une méthode de saisie."

#: gnome-initial-setup/pages/keyboard/input-chooser.ui:8
msgid "Search keyboards and input methods"
msgstr "Rechercher des claviers et des méthodes de saisie"

#: gnome-initial-setup/pages/language/cc-language-chooser.c:242
msgid "No languages found"
msgstr "Aucune langue trouvée"

#: gnome-initial-setup/pages/language/gis-language-page.c:262
msgid "Welcome"
msgstr "Bienvenue"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it.
#.
#: gnome-initial-setup/pages/language/gis-welcome-widget.c:132
msgid "Welcome!"
msgstr "Bienvenue !"

#: gnome-initial-setup/pages/network/gis-network-page.c:297
msgctxt "Wireless access point"
msgid "Other…"
msgstr "Autre…"

#: gnome-initial-setup/pages/network/gis-network-page.c:379
msgid "Wireless networking is disabled"
msgstr "Le réseau sans fil est désactivé"

#: gnome-initial-setup/pages/network/gis-network-page.c:386
msgid "Checking for available wireless networks"
msgstr "Recherche de réseaux sans fil en cours"

#: gnome-initial-setup/pages/network/gis-network-page.c:781
msgid "Network"
msgstr "Réseau"

#: gnome-initial-setup/pages/network/gis-network-page.ui:15
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: gnome-initial-setup/pages/network/gis-network-page.ui:16
msgid ""
"Connecting to the internet helps you get new apps, information, and other "
"upgrades. It also helps set the time and your location automatically."
msgstr ""
"La connexion à Internet vous permet d’obtenir de nouvelles applications, des "
"informations et d’autres mises à jour. Cela vous permet également de "
"configurer automatiquement l’heure et votre emplacement."

#: gnome-initial-setup/pages/network/gis-network-page.ui:56
msgid "No wireless available"
msgstr "Aucun réseau sans fil disponible"

#: gnome-initial-setup/pages/network/gis-network-page.ui:69
msgid "Turn On"
msgstr "Activer"

#. Translators: The placeholder is the user’s full name.
#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:102
#, c-format
msgid "Parental Controls for %s"
msgstr "Contrôles parentaux pour %s"

#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:104
msgid "Set restrictions on what this user can run or install."
msgstr ""
"Définir les restrictions sur ce que cet utilisateur peut exécuter ou "
"installer."

#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:199
msgid "Parental Controls"
msgstr "Contrôles parentaux"

#. Don’t break UI compatibility if parental controls are disabled.
#: gnome-initial-setup/pages/password/gis-password-page.c:90
msgid "Set a Password"
msgstr "Définir un mot de passe"

#: gnome-initial-setup/pages/password/gis-password-page.c:91
#: gnome-initial-setup/pages/password/gis-password-page.c:100
msgid "Be careful not to lose your password."
msgstr "Attention de ne pas égarer votre mot de passe."

#. Translators: The placeholder is for the user’s full name.
#: gnome-initial-setup/pages/password/gis-password-page.c:98
#, c-format
msgid "Set a Password for %s"
msgstr "Définir un mot de passe pour %s"

#: gnome-initial-setup/pages/password/gis-password-page.c:106
msgid "Set a Parent Password"
msgstr "Définir un mot de passe Parent"

#. Translators: The placeholder is the full name of the child user on the system.
#: gnome-initial-setup/pages/password/gis-password-page.c:108
#, c-format
msgid "This password will control access to the parental controls for %s."
msgstr "Ce mot de passe contrôlera l’accès aux contrôles parentaux pour %s."

#: gnome-initial-setup/pages/password/gis-password-page.c:243
msgid "The passwords do not match."
msgstr "Les mots de passe ne correspondent pas."

#: gnome-initial-setup/pages/password/gis-password-page.c:437
msgid "Password"
msgstr "Mot de passe"

#: gnome-initial-setup/pages/password/gis-password-page.ui:62
msgid "_Confirm Password"
msgstr "_Confirmer le mot de passe"

#: gnome-initial-setup/pages/password/pw-utils.c:81
msgctxt "Password hint"
msgid "The new password needs to be different from the old one."
msgstr "Le nouveau mot de passe doit être différent de l’ancien."

#: gnome-initial-setup/pages/password/pw-utils.c:83
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing some letters "
"and numbers."
msgstr ""
"Ce mot de passe est très proche de l’ancien. Essayez de changer des lettres "
"et des chiffres."

#: gnome-initial-setup/pages/password/pw-utils.c:85
#: gnome-initial-setup/pages/password/pw-utils.c:93
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing the password a "
"bit more."
msgstr ""
"Ce mot de passe est très proche de l’ancien. Essayez de le modifier un peu "
"plus."

#: gnome-initial-setup/pages/password/pw-utils.c:87
msgctxt "Password hint"
msgid ""
"This is a weak password. A password without your user name would be stronger."
msgstr ""
"Ce mot de passe est faible. Un mot de passe sans votre nom d’utilisateur "
"serait plus robuste."

#: gnome-initial-setup/pages/password/pw-utils.c:89
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid using your name in the password."
msgstr ""
"Ce mot de passe est faible. Évitez d’utiliser votre nom dans le mot de passe."

#: gnome-initial-setup/pages/password/pw-utils.c:91
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid some of the words included in the "
"password."
msgstr ""
"Ce mot de passe est faible. Évitez certains mots contenus dans le mot de "
"passe."

#: gnome-initial-setup/pages/password/pw-utils.c:95
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid common words."
msgstr "Ce mot de passe est faible. Évitez d’utiliser des mots usuels."

#: gnome-initial-setup/pages/password/pw-utils.c:97
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid reordering existing words."
msgstr "Ce mot de passe est faible. Évitez de réarranger des mots existants."

#: gnome-initial-setup/pages/password/pw-utils.c:99
msgctxt "Password hint"
msgid "This is a weak password. Try to use more numbers."
msgstr "Ce mot de passe est faible. Utilisez plus de chiffres."

#: gnome-initial-setup/pages/password/pw-utils.c:101
msgctxt "Password hint"
msgid "This is a weak password. Try to use more uppercase letters."
msgstr "Ce mot de passe est faible. Utilisez plus de lettres majuscules."

#: gnome-initial-setup/pages/password/pw-utils.c:103
msgctxt "Password hint"
msgid "This is a weak password. Try to use more lowercase letters."
msgstr "Ce mot de passe est faible. Utilisez plus de lettres minuscules."

#: gnome-initial-setup/pages/password/pw-utils.c:105
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use more special characters, like "
"punctuation."
msgstr ""
"Ce mot de passe est faible. Utilisez plus de caractères spéciaux, comme de "
"la ponctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:107
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."
msgstr ""
"Ce mot de passe est faible. Utilisez un mélange de lettres, de chiffres et "
"de ponctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:109
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid repeating the same character."
msgstr "Ce mot de passe est faible. Évitez de répéter le même caractère."

#: gnome-initial-setup/pages/password/pw-utils.c:111
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid repeating the same type of character: "
"you need to mix up letters, numbers and punctuation."
msgstr ""
"Ce mot de passe est faible. Évitez de répéter les mêmes types de "
"caractères : mélangez des lettres, des chiffres et de la ponctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:113
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid sequences like 1234 or abcd."
msgstr "Ce mot de passe est faible. Évitez des suites comme 1234 ou abcd."

#: gnome-initial-setup/pages/password/pw-utils.c:115
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to add more letters, numbers and punctuation."
msgstr ""
"Ce mot de passe est faible. Le mot de passe doit être plus long. Ajoutez "
"plus de lettres, de chiffres ou de ponctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:117
msgctxt "Password hint"
msgid "Mix uppercase and lowercase and try to use a number or two."
msgstr "Mélangez majuscules et minuscules et ajoutez-y un chiffre ou deux."

#: gnome-initial-setup/pages/password/pw-utils.c:119
msgctxt "Password hint"
msgid ""
"Adding more letters, numbers and punctuation will make the password stronger."
msgstr ""
"L’ajout de plus de lettres, de chiffres ou de ponctuation rendra le mot de "
"passe encore plus fort."

#. Translators: the first parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:72
#, c-format
msgid ""
"Sends technical reports that have personal information automatically "
"removed. Data is collected by %1$s (<a href='%2$s'>privacy policy</a>)."
msgstr ""
"Envoie des rapports techniques dont les informations personnelles sont "
"automatiquement supprimées. Les informations sont collectées par %1$s (<a "
"href='%2$s'>politique de confidentialité</a>)."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:82
#, c-format
msgid ""
"Sends technical reports that have personal information automatically "
"removed. Data is collected by %s."
msgstr ""
"Envoie des rapports techniques dont les informations personnelles sont "
"automatiquement supprimées. Les informations sont collectées par %s."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:200
msgid "Privacy Policy"
msgstr "Politique de confidentialité"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:235
#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:14
msgid "Privacy"
msgstr "Confidentialité"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:27
msgid "Location Services"
msgstr "Services de géolocalisation"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:28
msgid ""
"Allows applications to determine your geographical location. Uses the "
"Mozilla Location Service (<a href='https://location.services.mozilla.com/"
"privacy'>privacy policy</a>)."
msgstr ""
"Autorise les applications à géolocaliser votre position actuelle. Utilise le "
"service Mozilla Location (<a href='https://location.services.mozilla.com/"
"privacy'>politique de confidentialité</a>)."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:44
msgid "Automatic Problem Reporting"
msgstr "Envoi automatique de rapports de problèmes"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:62
msgid ""
"Privacy controls can be changed at any time from the Settings application."
msgstr ""
"Les paramètres de confidentialité sont modifiables à tout moment dans les "
"Paramètres."

#: gnome-initial-setup/pages/software/gis-software-page.c:109
#: gnome-initial-setup/pages/software/gis-software-page.ui:11
msgid "Third-Party Repositories"
msgstr "Dépôts de logiciels tiers"

#: gnome-initial-setup/pages/software/gis-software-page.c:110
msgid ""
"Third-party repositories provide access to additional software from selected "
"external sources, including popular apps and drivers that are important for "
"some devices. Some proprietary software is included."
msgstr ""
"Les dépôts tiers donnent accès à des logiciels supplémentaires à partir de "
"sources externes sélectionnées, comprenant des applications populaires et "
"des pilotes importants pour certains appareils. Cela peut inclure des "
"logiciels propriétaires."

#: gnome-initial-setup/pages/software/gis-software-page.c:120
msgid "_Disable Third-Party Repositories"
msgstr "_Désactiver les dépôts de logiciels tiers"

#: gnome-initial-setup/pages/software/gis-software-page.c:125
msgid "_Enable Third-Party Repositories"
msgstr "_Activer les dépôts de logiciels tiers"

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:232
#, c-format
msgid "_Start Using %s"
msgstr "_Commencer à utiliser %s"

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:239
#, c-format
msgid "%s is ready to be used. We hope that you love it!"
msgstr "%s est prêt à l’utilisation. Nous espérons que vous l’apprécierez !"

#: gnome-initial-setup/pages/summary/gis-summary-page.c:263
msgid "Setup Complete"
msgstr "Configuration terminée"

#: gnome-initial-setup/pages/summary/gis-summary-page.ui:7
msgid "All done!"
msgstr "Tout est terminé !"

#: gnome-initial-setup/pages/timezone/gis-location-entry.c:117
msgid "Search cities"
msgstr "Rechercher des villes"

#. Recurse, adding the ADM1 name to the country name
#. Translators: this is the name of a location followed by a region, for example:
#. * 'London, United Kingdom'
#. * You shouldn't need to translate this string unless the language has a different comma.
#.
#. <location> with no parent <city>
#. Translators: this is the name of a location followed by a region, for example:
#. * 'London, United Kingdom'
#. * You shouldn't need to translate this string unless the language has a different comma.
#.
#: gnome-initial-setup/pages/timezone/gis-location-entry.c:549
#: gnome-initial-setup/pages/timezone/gis-location-entry.c:577
#, c-format
msgid "%s, %s"
msgstr "%s, %s"

#. Translators: "city, country"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:271
#, c-format
msgctxt "timezone loc"
msgid "%s, %s"
msgstr "%s, %s"

#. Translators: UTC here means the Coordinated Universal Time.
#. * %:::z will be replaced by the offset from UTC e.g. UTC+02
#.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:308
msgid "UTC%:::z"
msgstr "UTC%:::z"

#. Translators: This is the time format used in 12-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:312
msgid "%l:%M %p"
msgstr "%l:%M %p"

#. Translators: This is the time format used in 24-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:315
msgid "%R"
msgstr "%R"

#. Translators: "timezone (utc shift)"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:318
#, c-format
msgctxt "timezone map"
msgid "%s (%s)"
msgstr "%s (%s)"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:484
#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:12
msgid "Time Zone"
msgstr "Fuseau horaire"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:13
msgid ""
"The time zone will be set automatically if your location can be found. You "
"can also search for a city to set it yourself."
msgstr ""
"Le fuseau horaire sera automatiquement paramétré si votre position est "
"détectée. Vous pouvez également rechercher une ville pour le paramétrer vous-"
"même."

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:44
msgid "Please search for a nearby city"
msgstr "Rechercher une ville voisine"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it. The space
#. * before the exclamation mark in this string is a typographical thin
#. * space (U200a) to improve the spacing in the title, which you can
#. * keep or remove. The %s is getting replaced with the name and version
#. * of the OS, e.g. "GNOME 3.38"
#.
#: gnome-initial-setup/pages/welcome/gis-welcome-page.c:75
#, c-format
msgid "Welcome to %s !"
msgstr "Bienvenue dans %s !"

#: gnome-initial-setup/pages/welcome/gis-welcome-page.ui:4
msgid "Setup"
msgstr "Configuration"

#: gnome-initial-setup/pages/welcome/gis-welcome-page.ui:39
msgid ""
"Setup will guide you through making an account and enabling some features. "
"We’ll have you up and running in no time."
msgstr ""
"L’assistant de configuration va vous guider pour la création d’un compte et "
"l’activation de fonctionnalités. Vous allez être prêt en un rien de temps."

#: gnome-initial-setup/pages/welcome/gis-welcome-page.ui:49
msgid "_Start Setup"
msgstr "_Démarrer la configuration"

#~ msgid "Take a Picture…"
#~ msgstr "Prendre une image…"

#~ msgid "Avatar image"
#~ msgstr "Image d’avatar"

#~ msgid "_Confirm"
#~ msgstr "_Confirmer"

#~ msgid "This is a weak password."
#~ msgstr "Ce mot de passe est faible."

#, c-format
#~ msgid ""
#~ "Sending reports of technical problems helps us to improve %s. Reports are "
#~ "sent anonymously and are scrubbed of personal data."
#~ msgstr ""
#~ "Envoyer des rapports sur des problèmes survenus nous aide à améliorer %s. "
#~ "Ces rapports nous parviennent anonymement et sont épurés de toute donnée "
#~ "personnelle vous concernant."

#, c-format
#~ msgid "Problem data will be collected by %s:"
#~ msgstr "Les données des problèmes seront collectées par %s :"

#~ msgid "Uses Mozilla Location Service:"
#~ msgstr "Utilise le service de géolocalisation de Mozilla :"

#~ msgid "Loading…"
#~ msgstr "Chargement…"

#~ msgid ""
#~ "Connecting to the Internet will enable you to set the time, add your "
#~ "details, and enable you to access your email, calendar, and contacts. It "
#~ "is also necessary for enterprise login accounts."
#~ msgstr ""
#~ "En vous connectant à Internet, vous pouvez mettre l’horloge à l’heure, "
#~ "ajouter vos informations, accéder à vos courriels, votre agenda et vos "
#~ "contacts. Cela est aussi indispensable pour les comptes d’entreprise."

#~ msgid "License Agreements"
#~ msgstr "Contrat de licence"

#~ msgid "No regions found"
#~ msgstr "Aucune région trouvée"

#~ msgid "Region"
#~ msgstr "Région"

#~ msgid "Choose your country or region."
#~ msgstr "Choisir un emplacement ou une région."
